package br.com.guilherme.lembrol.views.formulariosSenha;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import br.com.guilherme.lembrol.R;
import br.com.guilherme.lembrol.models.senha.SenhaItemTable;
import br.com.guilherme.lembrol.objects.SenhaItem;
import br.com.guilherme.lembrol.validators.SenhaValidator;
import br.com.guilherme.lembrol.views.telaPrincipal.TelaPrincipal;

public class EditarSenha extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "EditarSenha";

    SenhaItem senhaItem;

    EditText editTextTitulo;
    EditText editTextSenha;
    EditText editTextDetalhe;
    Button btnEditarSenha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_senha);

        getSupportActionBar().setTitle(R.string.editar_senha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        senhaItem = new SenhaItem();

        long codigo = getIntent().getLongExtra("codigo", 0);
        buscarSenha(codigo);

        btnEditarSenha = (Button) findViewById(R.id.btnEditarSenha);
        btnEditarSenha.setOnClickListener(this);
    }

    private void buscarSenha(long codigo) {
        SenhaItemTable table = new SenhaItemTable(this);
        senhaItem = table.obterItem(codigo);

        instanciarFormulario();
        editTextTitulo.setText(senhaItem.getTitulo());
        editTextSenha.setText(senhaItem.getSenha());
        editTextDetalhe.setText(senhaItem.getDetalhe());
    }

    @Override
    public void onClick(View v) {
        TextView textViewErros = (TextView) findViewById(R.id.textViewErros);
        textViewErros.setText("");

        buscarDadosDoFormulario();

        try {
            SenhaValidator.validar(this, senhaItem);

            SenhaItemTable table = new SenhaItemTable(this);
            table.atualizar(senhaItem);

            Toast.makeText(getApplicationContext(), R.string.registro_salvo, Toast.LENGTH_LONG).show();
            startActivity(new Intent(this, TelaPrincipal.class));
        } catch (Exception e) {
            Log.w(TAG, e.getMessage());
            textViewErros.setText(e.getMessage());
        }
    }

    private void buscarDadosDoFormulario() {
        senhaItem.setTitulo(editTextTitulo.getText().toString());
        senhaItem.setSenha(editTextSenha.getText().toString());
        senhaItem.setDetalhe(editTextDetalhe.getText().toString());
    }

    private void instanciarFormulario() {
        editTextTitulo = (EditText) findViewById(R.id.editTextTitulo);
        editTextSenha = (EditText) findViewById(R.id.editTextSenha);
        editTextDetalhe = (EditText) findViewById(R.id.editTextDetalhe);
    }

    private void removerSenha() {
        try {
            SenhaItemTable table = new SenhaItemTable(this);
            table.remover(senhaItem);

            Toast.makeText(getApplicationContext(), "Item removido com sucesso", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(this, TelaPrincipal.class));
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.removerSenha:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Atenção")
                        .setMessage("Deseja remover esse item?")
                        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                removerSenha();
                            }
                        })
                        .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // nothing
                            }
                        }).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
