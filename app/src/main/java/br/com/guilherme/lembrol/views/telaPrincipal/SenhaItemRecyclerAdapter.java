package br.com.guilherme.lembrol.views.telaPrincipal;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.com.guilherme.lembrol.R;
import br.com.guilherme.lembrol.objects.SenhaItem;

class SenhaItemRecyclerAdapter extends RecyclerView.Adapter<SenhaItemViewHolder> {
    private List<SenhaItem> mLista;

    SenhaItemRecyclerAdapter(List<SenhaItem> lista) {
        mLista = lista;
    }

    @Override
    public SenhaItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_view_senha_item, parent, false);
        return new SenhaItemViewHolder(layoutView);
    }

    public void onBindViewHolder(SenhaItemViewHolder holder, int position) {
        SenhaItem senhaItem = mLista.get(position);
        holder.textViewTitulo.setText(senhaItem.getTitulo());
        holder.textViewDetalhe.setText(senhaItem.getDetalhe());
    }

    @Override
    public int getItemCount() {
        return mLista.size();
    }

    SenhaItem getItem(int position) {
        return mLista.get(position);
    }

    public void clear() {
        mLista.clear();
    }

    public void addAll(List<SenhaItem> lista) {
        mLista.addAll(lista);
    }
}
