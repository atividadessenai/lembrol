package br.com.guilherme.lembrol.objects;

public class SenhaItem {
    private long id;
    private String titulo;
    private String senha;
    private String detalhe;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getDetalhe() {
        return detalhe;
    }

    public void setDetalhe(String detalhe) {
        this.detalhe = detalhe;
    }

    @Override
    public String toString() {
        return "SenhaItem{" +
                "id=" + id +
                ", titulo='" + titulo + '\'' +
                ", senha='" + senha + '\'' +
                ", detalhe='" + detalhe + '\'' +
                '}';
    }
}
