package br.com.guilherme.lembrol.validators;

import android.content.Context;

import br.com.guilherme.lembrol.R;
import br.com.guilherme.lembrol.objects.SenhaItem;

public class SenhaValidator {
    public static void validar(Context context, SenhaItem senhaItem) throws Exception {
        String mensagem = "";

        if (senhaItem.getTitulo().isEmpty()) {
            mensagem += String.format(context.getString(R.string.validacao_nao_informado), context.getString(R.string.titulo));
        }

        if (senhaItem.getSenha().isEmpty()) {
            mensagem += String.format(context.getString(R.string.validacao_nao_informado), context.getString(R.string.senha));
        }

        if (senhaItem.getDetalhe().isEmpty()) {
            mensagem += String.format(context.getString(R.string.validacao_nao_informado), context.getString(R.string.detalhe));
        }

        if (!mensagem.isEmpty()) {
            throw new Exception(mensagem);
        }
    }
}
