package br.com.guilherme.lembrol.models.senha;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.com.guilherme.lembrol.models.DataBaseHelper;
import br.com.guilherme.lembrol.objects.SenhaItem;

public class SenhaItemTable {

    private SQLiteDatabase mDataBase;
    private DataBaseHelper mDataHelper;

    private static final String TABLE = "SENHA";
    private static final String COLUMN_ID = "ID";
    private static final String COLUMN_TITULO = "NOME";
    private static final String COLUMN_SENHA = "SENHA";
    private static final String COLUMN_DETALHE = "DETALHE";

    private static final String[] ALL_COLUMNS = {
        COLUMN_ID, COLUMN_TITULO, COLUMN_SENHA, COLUMN_DETALHE
    };

    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE
            + "("
            + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_TITULO + " TEXT, "
            + COLUMN_SENHA+ " TEXT, "
            + COLUMN_DETALHE + " TEXT"
            +")";

    public static final String DROP_TABLE = "DROP TABLE " + TABLE;

    public SenhaItemTable(Context context) {
        mDataHelper = DataBaseHelper.getInstance(context);
        open();
    }

    private void open() {
        mDataBase = mDataHelper.getWritableDatabase();
    }

    private SenhaItem toObject(Cursor cursor) {
        SenhaItem senhaItem = new SenhaItem();
        senhaItem.setId(cursor.getLong(cursor.getColumnIndex(COLUMN_ID)));
        senhaItem.setTitulo(cursor.getString(cursor.getColumnIndex(COLUMN_TITULO)));
        senhaItem.setSenha(cursor.getString(cursor.getColumnIndex(COLUMN_SENHA)));
        senhaItem.setDetalhe(cursor.getString(cursor.getColumnIndex(COLUMN_DETALHE)));
        return senhaItem;
    }

    private ContentValues toContent(SenhaItem senhaItem) {
        ContentValues content = new ContentValues();
        content.put(COLUMN_TITULO, senhaItem.getTitulo());
        content.put(COLUMN_SENHA, senhaItem.getSenha());
        content.put(COLUMN_DETALHE, senhaItem.getDetalhe());
        return content;
    }

    public long inserir(SenhaItem senhaItem) {
        return mDataBase.insert(TABLE, null, toContent(senhaItem));
    }

    public int atualizar(SenhaItem senhaItem) {
        return mDataBase.update(TABLE, toContent(senhaItem), COLUMN_ID + "=?", new String[]{String.valueOf(senhaItem.getId())});
    }

    public void remover(SenhaItem senhaItem) {
        mDataBase.delete(TABLE, COLUMN_ID + "=?", new String[]{String.valueOf(senhaItem.getId())});
    }

    public List<SenhaItem> obterTodos() {
        Cursor cursor = null;
        List<SenhaItem> senhas = new ArrayList<>();

        try {
            cursor = mDataBase.query(TABLE, ALL_COLUMNS, null, null, null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    senhas.add(toObject(cursor));
                } while (cursor.moveToNext());
            }
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return senhas;
    }

    public SenhaItem obterItem(Long id) {
        Cursor cursor = null;
        SenhaItem senha = new SenhaItem();
        try {
            cursor = mDataBase.query(TABLE, ALL_COLUMNS, COLUMN_ID + "=?", new String[]{String.valueOf(id)}, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();

                senha = toObject(cursor);
            }
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return senha;
    }
}
