package br.com.guilherme.lembrol.views.telaPrincipal;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import java.util.List;

import br.com.guilherme.lembrol.R;
import br.com.guilherme.lembrol.helpers.RecyclerItemClickListener;
import br.com.guilherme.lembrol.models.DataBaseHelper;
import br.com.guilherme.lembrol.models.senha.SenhaItemTable;
import br.com.guilherme.lembrol.objects.SenhaItem;
import br.com.guilherme.lembrol.views.formulariosSenha.CadastrarSenha;
import br.com.guilherme.lembrol.views.formulariosSenha.VisualizarSenha;

public class TelaPrincipal extends AppCompatActivity {
    private static final String TAG = "TelaPrincipal";
    private SenhaItemRecyclerAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_principal);

        RecyclerView recyclerSenhas = (RecyclerView) findViewById(R.id.recyclerSenhas);
        recyclerSenhas.setLayoutManager(new LinearLayoutManager(this));

        mAdapter = new SenhaItemRecyclerAdapter(obterListaDeSenhas());
        recyclerSenhas.setAdapter(mAdapter);
        recyclerSenhas.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
            onItemCliclado(position);
            }
        }));
        
        DataBaseHelper.getInstance(this);

        FloatingActionButton floatButton = (FloatingActionButton) findViewById(R.id.floatButtonAdicionar);
        floatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CadastrarSenha.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAdapter.clear();
        mAdapter.addAll(obterListaDeSenhas());
        mAdapter.notifyDataSetChanged();
    }

    private List<SenhaItem> obterListaDeSenhas() {
        SenhaItemTable table = new SenhaItemTable(this);
        return table.obterTodos();
    }

    private void onItemCliclado(int position) {
        long codigo = mAdapter.getItem(position).getId();

        Log.i(TAG, "Item selecionado: " + String.valueOf(codigo));
        startActivity(new Intent(getApplicationContext(), VisualizarSenha.class).putExtra("codigo", codigo));
    }
}
