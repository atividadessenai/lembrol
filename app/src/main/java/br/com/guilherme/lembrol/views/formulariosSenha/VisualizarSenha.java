package br.com.guilherme.lembrol.views.formulariosSenha;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import br.com.guilherme.lembrol.R;
import br.com.guilherme.lembrol.models.senha.SenhaItemTable;
import br.com.guilherme.lembrol.objects.SenhaItem;
import br.com.guilherme.lembrol.views.telaPrincipal.TelaPrincipal;

public class VisualizarSenha extends AppCompatActivity {
    private static final String TAG = "VisualizarSenha";
    SenhaItem senhaItem;

    TextView textViewTitulo;
    TextView textViewSenha;
    TextView textViewDetalhe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualizar_senha);

        getSupportActionBar().setTitle("Visualizar senha");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        senhaItem = new SenhaItem();
        long codigo = getIntent().getLongExtra("codigo", 0);
        buscarSenha(codigo);
    }

    private void buscarSenha(long codigo) {
        SenhaItemTable table = new SenhaItemTable(this);
        senhaItem = table.obterItem(codigo);

        prepararTela();
        textViewTitulo.setText(senhaItem.getTitulo());
        textViewSenha.setText(senhaItem.getSenha());
        textViewDetalhe.setText(senhaItem.getDetalhe());

        Log.i(TAG, senhaItem.toString());
    }

    private void prepararTela() {
        textViewTitulo = (TextView) findViewById(R.id.textViewTitulo);
        textViewSenha = (TextView) findViewById(R.id.textViewSenha);
        textViewDetalhe = (TextView) findViewById(R.id.textViewDetalhe);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.view_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.editarSenha:
                Intent intent = new Intent(this, EditarSenha.class);
                intent.putExtra("codigo", senhaItem.getId());
                startActivity(intent);
                return true;
            case R.id.removerSenha:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
