package br.com.guilherme.lembrol.models;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import br.com.guilherme.lembrol.models.senha.SenhaItemTable;

public class DataBaseHelper extends SQLiteOpenHelper {
    private static DataBaseHelper sInstance;

    private static final String DATABASE_NAME = "lembrol.db";
    private static final int DATABASE_VERSION = 1;

    private DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized DataBaseHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new DataBaseHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SenhaItemTable.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SenhaItemTable.DROP_TABLE);
        onCreate(db);
    }
}
