package br.com.guilherme.lembrol.views.telaPrincipal;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import br.com.guilherme.lembrol.R;

class SenhaItemViewHolder extends RecyclerView.ViewHolder {
    TextView textViewTitulo;
    TextView textViewDetalhe;

    SenhaItemViewHolder(View itemView) {
        super(itemView);

        textViewTitulo = (TextView)itemView.findViewById(R.id.textViewTitulo);
        textViewDetalhe = (TextView)itemView.findViewById(R.id.textViewDetalhe);
    }
}
