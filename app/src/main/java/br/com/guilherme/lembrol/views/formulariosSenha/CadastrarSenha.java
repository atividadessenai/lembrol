package br.com.guilherme.lembrol.views.formulariosSenha;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import br.com.guilherme.lembrol.R;
import br.com.guilherme.lembrol.models.senha.SenhaItemTable;
import br.com.guilherme.lembrol.objects.SenhaItem;
import br.com.guilherme.lembrol.validators.SenhaValidator;
import br.com.guilherme.lembrol.views.telaPrincipal.TelaPrincipal;

public class CadastrarSenha extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "CadastrarSenhaActivity";

    Button btnCadastrarSenha;
    EditText titulo;
    EditText senha;
    EditText detalhe;

    SenhaItem senhaItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar_senha);

        getSupportActionBar().setTitle(R.string.cadastrar_senha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        instanciarFormulario();

        senhaItem = new SenhaItem();

        btnCadastrarSenha = (Button) findViewById(R.id.btnCadastrarSenha);
        btnCadastrarSenha.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        try {
            buscarDadosDoFormulario();
            SenhaValidator.validar(this, senhaItem);

            SenhaItemTable table = new SenhaItemTable(this);
            table.inserir(senhaItem);

            Toast.makeText(getApplicationContext(), R.string.registro_salvo, Toast.LENGTH_LONG).show();
            startActivity(new Intent(this, TelaPrincipal.class));
        } catch (Exception e) {
            Log.w(TAG, e.getMessage());

            TextView textViewErros = (TextView) findViewById(R.id.textViewErros);
            textViewErros.setText(e.getMessage());
        }
    }

    private void buscarDadosDoFormulario() {
        senhaItem.setTitulo(titulo.getText().toString());
        senhaItem.setSenha(senha.getText().toString());
        senhaItem.setDetalhe(detalhe.getText().toString());
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private void instanciarFormulario() {
        titulo = (EditText) findViewById(R.id.editTextTitulo);
        senha = (EditText) findViewById(R.id.editTextSenha);
        detalhe = (EditText) findViewById(R.id.editTextDetalhe);
    }
}
